const noblox = require('noblox.js')
const http = require('http')
async function setCookie() {
    var currentUser = await noblox.setCookie('') 
    return currentUser
}

async function WebStart() {
    const express = require("express")
    const bodyParser = require('body-parser')
    const app = express()
    app.use(bodyParser())
    app.listen(80, () => {
        console.log("Running")
    })
    app.get("/", (req, res) => {
        res.send("I love Javascriptz")
    })
    app.post("/update", async(req,res) => {
        var userId = req.body.userId
        var rank = req.body.rank
        await setCookie()
        noblox.setRank(4984149, userId, rank).then(() => {
            res.json({ success: true })
            }).catch(err => {
            res.json({ success: false, error: err })
            })
    })
    setInterval(async() => {
        http.get("http://noblox.js.org/")
    }, 100000)
}

async function StartApp() {
    const currentUser = await setCookie()
    console.log(`Logged in as ${currentUser.UserName} [${currentUser.UserID}]`)
    WebStart()
    //Test()
}
StartApp()
process.on('uncaughtException', (err) => {
    console.error('There was an uncaught error', err)
})
process.on('unhandledRejection', (reason, promise) => {
    console.log('Unhandled Rejection at:', promise, 'reason:', reason);
    // Application specific logging, throwing an error, or other logic here
})